﻿using RestAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace RestAPI_Tests
{
    public class MongoCRUD_Tests
    {
        MongoCRUD db = new MongoCRUD("DatabaseTesting");
        string collection = "Tests";

        [Fact]
        public void CheckInsertAndReadMethod()
        {
            string testingData = Guid.NewGuid().ToString();
            var data = new CityWithRestaurants() { Url = testingData, Name = testingData };
            db.InsertRecord(collection, data);

            var sources = db.GetCityWithRestaurants<CityWithRestaurants>(collection);
            Assert.Equal(data.Name, sources.Find(x => x.Name.Equals(data.Name)).Name);
        }

        [Theory]
        [InlineData(2)]
        [InlineData(4)]
        public void CheckReturnCountOfItems(int count)
        {
            string testedCollection = Guid.NewGuid().ToString();

            for (int i = 0; i < count; i++)
            {
                string testingData = Guid.NewGuid().ToString();
                var data = new CityWithRestaurants() { Url = testingData, Name = testingData };
                db.InsertRecord(testedCollection, data);
            }

            var CountOfDatabaseItems = db.GetCountOfCity(testedCollection);

            Assert.Equal(count,CountOfDatabaseItems);

        }

        [Theory]
        [InlineData(2)]
        [InlineData(4)]
        [InlineData(5)]
        public void GetXItemsFromDatabase(int count)
        {
            string testedCollection = Guid.NewGuid().ToString();

            for (int i = 0; i < 4; i++)
            {
                string testingData = Guid.NewGuid().ToString();
                var data = new CityWithRestaurants() { Url = testingData, Name = testingData };
                db.InsertRecord(testedCollection, data);
            }

            var CountOfDatabaseItems = db.ReturnXRSSItems<CityWithRestaurants>(testedCollection, count);

            if (count <=4)
            {
                Assert.Equal(count, CountOfDatabaseItems.Count);
            }
            else
            {
                Assert.NotEqual(count, CountOfDatabaseItems.Count);
            }

        }

        [Fact]
        public void CheckReturnCityByName()
        {
            string testedCollection = Guid.NewGuid().ToString();
            string testingData = Guid.NewGuid().ToString();
            var data = new CityWithRestaurants() { Url = testingData, Name = testingData };
            db.InsertRecord(collection, data);

            var sources = db.returnCityWithRestaurantsFindedByCity<CityWithRestaurants>(collection, data.Name);
            Assert.Equal(data.Name, sources.Find(x => x.Name.Equals(data.Name)).Name);

        }

    }
}
