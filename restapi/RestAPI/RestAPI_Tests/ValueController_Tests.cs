﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using RestAPI.Controllers;
using RestAPI.Models;
using System.Windows.Markup;

namespace RestAPI_Tests
{
   public class ValueController_Tests
    {
        ValuesController vc = new ValuesController();
  
        //Bartek
        [Theory]
        [InlineData(100)]
        [InlineData(10000)]
        public void CheckTheDistanceParametr_Test(int distance)
        {

           int expected = vc.CheckTheDistanceParametr(distance);

            Assert.Equal(expected, distance);

        }
        //Bartek
        [Theory]
        [InlineData(21.05)]
        [InlineData(70.5)]
        public void CheckTheLattParametr_Test(double latt)
        {

            double expected = vc.CheckTheLattParametr(latt);

            Assert.Equal(expected, latt);

        }

        [Theory]
        [InlineData(21.05)]
        [InlineData(70.5)]
        //Bartek
        public void CheckTheLongTParametr_Test(double longT)
        {

            double expected = vc.CheckTheLongTParametr(longT);

            Assert.Equal(expected, longT);

        }
       
        [Theory]
        [InlineData(50000.5)]
        //Dominik
        public void CheckTheLattParametr_Test_Exception(double latt)
        {
            Assert.Throws<Exception>(() => vc.CheckTheLattParametr(latt));

        }
        //Dominik
        [Fact]
        public void AddToList_Test()
        {
            //Arrange
            Restaurant restaurant = new Restaurant();
            List<Restaurant> restaurants = new List<Restaurant>();

            int expect = 1;

            //Act

            vc.AddToList(restaurants, restaurant);

            //Assert
            Assert.True(restaurants.Count == expect);
        }

        [Theory]
        [InlineData(0)]
        //Dominik
        public void CheckTheDistanceParametr_Test_Exception(int distance)
        {
            Assert.Throws<Exception>(() => vc.CheckTheDistanceParametr(distance));

        }
       

        [Theory]
        [InlineData("Polska")]
        [InlineData("Ruska")]
        [InlineData("Kremowki")]
        //Michał
        public void CheckTheKitchenCorrect(string kitchen)
        {
            string expected = vc.CheckCorrectOfKitchen(kitchen);

            Assert.Equal(expected, kitchen);

        }
        [Fact]
        //Michał
        public void CheckTheKitchenCorrect_Exception()
        {
            string kitchen = "";
            Assert.Throws<Exception>(() => vc.CheckCorrectOfKitchen(kitchen));

        }

        [Theory]
        [InlineData(50000.5)]
        //Michał
        public void CheckTheLongTParametr_Test_Exception(double longT)
        {
            Assert.Throws<Exception>(() => vc.CheckTheLongTParametr(longT));

        }




    }
}

