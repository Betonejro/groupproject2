﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using RestAPI.Models;

namespace RestAPI_Tests
{
    public class Restaurants_Tests
    {
        Restaurant r = new Restaurant();
        [Fact]
        public void CheckTheName_Exception()
        {
            string name = "";
            Assert.Throws<Exception>(() => r.CheckTheName(name));
        }

        [Theory]
        [InlineData("KremowkiZwadowic")]
        [InlineData("PiwiarniaUZbyszka")]
        public void CheckTheName(string name)
        {
            string expected = r.CheckTheName(name);
            Assert.Equal(expected, name);
        }

        [Fact]
        public void CheckTheRate_Exception()
        {
            double rate = 50;
            Assert.Throws<Exception>(() => r.CheckRate(50));
        }

    }
}
