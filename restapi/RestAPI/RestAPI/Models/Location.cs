﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RestAPI.Models
{
    public class Location
    {

        public string latt { get; set; }
        public string longt { get; set; }

        public class Standard
        {
            public string addresst { get; set; }
            public string city { get; set; }
            public string prov { get; set; }
            public string countryname { get; set; }
            public string postal { get; set; }
            public string confidence { get; set; }
            public string latt { get; set; }
            public string longt { get; set; }
        }


        public class Loc
        {
            public string longt { get; set; }
            public string prov { get; set; }
            public string city { get; set; }
            public string countryname { get; set; }
            public string postal { get; set; }
            public string region { get; set; }
            public string latt { get; set; }
        }

        public class Alt
        {
            public Loc loc { get; set; }
        }


        public class RootObject
        {
            public string statename { get; set; }
            public string distance { get; set; }
            public string elevation { get; set; }
            public string state { get; set; }
            public string latt { get; set; }
            public string city { get; set; }
            public string prov { get; set; }
            public string geocode { get; set; }
            public string geonumber { get; set; }
            public string country { get; set; }
            public string stnumber { get; set; }
            public string staddress { get; set; }
            public string inlatt { get; set; }
            public Alt alt { get; set; }
            public string timezone { get; set; }
            public string region { get; set; }
            public string postal { get; set; }
            public string longt { get; set; }
            public string remaining_credits { get; set; }
            public string confidence { get; set; }
            public string inlongt { get; set; }
            public string @class { get; set; }
            public string altgeocode { get; set; }
        }
    }
}