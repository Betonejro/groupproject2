﻿using RestAPI.App_Start;
using Newtonsoft.Json;
using System.Collections.Generic;
using System;
using System.Text.RegularExpressions;
using RestSharp;

namespace RestAPI.Models
{
    public class GeoCode
    {
        public Location Geocode(string address)
        {
            var normalizedAddress = NormalizeAddress(address);
            var client = new RestSharp.RestClient("https://geocode.xyz/");
            
            var req = new RestSharp.RestRequest("{address}&auth={key}?json=1", RestSharp.Method.GET)
                .AddParameter("address", normalizedAddress, ParameterType.UrlSegment)
                .AddParameter("key", GeoCodeConfig.Key, ParameterType.UrlSegment);

            var res = client.Execute(req);


            if (res.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var content = JsonConvert.DeserializeObject<Location>(res.Content);
                return content;
            }
            else
            {
                return Geocode(address);
            }

        }
        
        private string NormalizeAddress(string fullAddress)
        {
            string street = Regex.Replace(fullAddress, " ", "+").Trim();
            street = Regex.Replace(street, "/", "-");
            return street;
        }

    }
}