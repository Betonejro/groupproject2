﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MongoDB.Driver.GeoJsonObjectModel;
using Microsoft.Ajax.Utilities;

namespace RestAPI.Models
{
    public class RestaurantFinder
    {
        MongoCRUD mongoCRUD = new MongoCRUD("RestAPI");
        private const string CityCollection = "Miasta";
        private const string RestaurantCollection = "Restauracje";

        const string ListUrlHead = "https://pl.tripadvisor.com/Restaurants-g274723-oa";
        const string ListUrlTail = "-Poland.html#LOCATION_LIST";
        const string FirstPageListUrl = "https://pl.tripadvisor.com/Restaurants-g274723-Poland.html#LOCATION_LIST";
        const string MainSiteUrl = "https://pl.tripadvisor.com/Restaurants-g274723-oa20-Poland.html#LOCATION_LIST";
        const string UrlParent = "https://pl.tripadvisor.com";
        List<CityWithRestaurants> ListOfCityFromSite = new List<CityWithRestaurants>();
        List<Restaurant> ListOfRestaurantsData = new List<Restaurant>();
        public void ReadRestaurants()
        {
            Task task = new Task(() =>
            {
                DownloadCityUrl();
                SaveCityListToDatabase();
                ListOfCityFromSite = mongoCRUD.GetCityWithRestaurants<CityWithRestaurants>(CityCollection);

                DownloadRestaurantsFromCitys();
            });
            task.Start();
        }

        public void DownloadRestaurantsFromCitys()
        {
            foreach (var city in ListOfCityFromSite)
            {
                DownloadRestaurantsData(city.Url);
            }
        }
        public void DownloadRestaurantsData(string cityUrl)
        {
            var web = new HtmlWeb();
            var doc = web.Load(cityUrl);
            var htmlCollection = doc.DocumentNode.SelectNodes("//div[@id='EATERY_LIST_CONTENTS']/div[@id='EATERY_SEARCH_RESULTS']/div[@id='component_2']/div[@class='restaurants-list-List__wrapper--3PzDL']/div[@class='_1llCuDZj']/span/div[@class='_1kNOY9zw']/div[@class='_2jF2URLh']/span/a");

            foreach (var current in htmlCollection)
            {
                var restaurant = new Restaurant();
                restaurant.Url = UrlParent + current.Attributes["href"].Value;
                restaurant.Name = GetRestaurantName(restaurant.Url);
                restaurant.Address = GetRestaurantAddress(restaurant.Url);
                restaurant.Kuchnia = GetRestaurantKitchen(restaurant.Url);
                restaurant.Rate = GetRestaurantRate(restaurant.Url);
                restaurant.Voices = GetRestaurantVoices(restaurant.Url);
               
                var geoCode = new GeoCode();
                var geoLocation = geoCode.Geocode(restaurant.Address);
                restaurant.LatT = TryConvertToDoubleWithDot(geoLocation.latt);
                restaurant.LongT = TryConvertToDoubleWithDot(geoLocation.longt);
                restaurant.Location = new GeoJsonPoint<GeoJson2DGeographicCoordinates>(new GeoJson2DGeographicCoordinates(restaurant.LongT, restaurant.LatT));

                mongoCRUD.InsertIndexedData(restaurant);
            }
        }



        public void TryInsertToDatabase()
        {
            foreach (var restaurant in ListOfRestaurantsData)
            {
                if (mongoCRUD.CheckRestaurantInCollection(RestaurantCollection, restaurant.Url) != true)
                {
                    mongoCRUD.InsertRecord<Restaurant>(RestaurantCollection, restaurant);
                }
            }

            ListOfRestaurantsData.Clear();
        }

        public string GetRestaurantName(string url)
        {
            var web = new HtmlWeb();
            var doc = web.Load(url);
            var htmlCollection = doc.DocumentNode.SelectSingleNode("//div[@class='restaurants-detail-top-info-TopInfo__topRow--lnyaZ']/h1");

            return HttpUtility.HtmlDecode(htmlCollection.InnerText);
        }

        public string GetRestaurantAddress(string url)
        {
            var web = new HtmlWeb();
            var doc = web.Load(url);
            var htmlCollection = doc.DocumentNode.SelectSingleNode("//div[@class='restaurants-detail-top-info-TopInfo__infoRow--2m8Mx']/span/span/a[@class='restaurants-detail-top-info-TopInfo__infoCellLink--2ZRPG']");
            return HttpUtility.HtmlDecode(htmlCollection.InnerText);
        }

        public double GetRestaurantRate(string url)
        {
            var web = new HtmlWeb();
            var doc = web.Load(url);
            var htmlNode = doc.DocumentNode.SelectSingleNode("//span[@class='restaurants-detail-overview-cards-RatingsOverviewCard__overallRating--nohTl']");
            double rate = TryConvertToDouble(htmlNode.InnerText);
            return rate;
        }

        public int GetRestaurantVoices(string url)
        {
            var web = new HtmlWeb();
            var doc = web.Load(url);
            var htmlNode = doc.DocumentNode.SelectSingleNode("//a[@class='restaurants-detail-overview-cards-RatingsOverviewCard__ratingCount--DFxkG']");
            int voices = TryConvertToInt(htmlNode.InnerText);
            return voices;
        }
        public List<string> GetRestaurantKitchen(string url)
        {
            List<string> kitchen = new List<string>();

            var web = new HtmlWeb();
            var doc = web.Load(url);
            var nodeStruct = "//div[@class='restaurants-detail-overview-cards-DetailOverviewCards__wrapperDiv--1Dfhf']/div/div[@class='restaurants-detail-overview-cards-DetailsSectionOverviewCard__detailsSummary--evhlS']/div";
            var nameStruct = ".//div[@class='restaurants-detail-overview-cards-DetailsSectionOverviewCard__categoryTitle--2RJP_']";
            var kitchenStruct = ".//div[@class='restaurants-detail-overview-cards-DetailsSectionOverviewCard__tagText--1OH6h']";
            var htmlNodes = doc.DocumentNode.SelectNodes(nodeStruct);

            foreach (var node in htmlNodes)
            {
                var name = node.SelectSingleNode(nameStruct);
                if (name.InnerText.ToString() == "KUCHNIE")
                {
                    var kuchnie = node.SelectSingleNode(kitchenStruct);

                    var listaKuchni = HttpUtility.HtmlDecode(kuchnie.InnerText).Split(',');

                    foreach (var kuchnia in listaKuchni)
                    {
                        kitchen.Add(kuchnia.Trim());
                    }
                }
            }

            return kitchen;
        }
        public void DownloadCityUrl()
        {
            var countOfCityInList = GetCountOfCityInListFromSite();
            if (countOfCityInList != mongoCRUD.GetCountOfCity(CityCollection))
            {
                for (int i = 0; i < CalculateCountOfPagesInList(countOfCityInList); i++)
                {
                    if (i == 0)
                    {
                        GetUrlToRestaurantsInCityFromFirstPage(FirstPageListUrl);
                    }
                    else
                    {
                        int page = 20 * i;
                        GetUrlToRestaurantsInCityFromListPage(ListUrlHead + page + ListUrlTail);
                    }
                }
            }
        }
        public void SaveCityListToDatabase()
        {
            foreach (var item in ListOfCityFromSite)
            {
                if (mongoCRUD.CheckCityInCollection(CityCollection, item.Url) != true)
                {
                    mongoCRUD.InsertRecord<CityWithRestaurants>(CityCollection, item);
                }
            }
        }
        private int GetCountOfCityInListFromSite()
        {
            var web = new HtmlWeb();
            var doc = web.Load(RestaurantFinder.MainSiteUrl);
            var htmlCollection = doc.DocumentNode.SelectNodes("//div[@id='LOCATION_LIST']/div[@class='deckTools']/div[@class='pagination']/span[@class='pgCount']");
            var htmlLinks = new StringBuilder();
            foreach (var current in htmlCollection)
            {
                htmlLinks.AppendLine(current.InnerText);
            }

            var separatedDiv = htmlLinks.ToString().Split('z');
            var count = TryConvertToInt(separatedDiv[separatedDiv.Length - 1]);

            return count;
        }

        private string GetNameOfCity(string fullname)
        {
            var name = Regex.Replace(fullname, "Restauracje - ", "").Trim();

            return name;
        }

        private int CalculateCountOfPagesInList(int countOfItems)
        {
            var countOfPages = Convert.ToInt32(Math.Ceiling((countOfItems * 1.0) / 20.0));

            return countOfPages;
        }

        private void GetUrlToRestaurantsInCityFromListPage(string url)
        {
            var web = new HtmlWeb();
            var doc = web.Load(url);

            var htmlCollection = doc.DocumentNode.SelectNodes("//div[@id='LOCATION_LIST']/ul[@class='geoList']/li/a");
            foreach (var current in htmlCollection)
            {
                var itemUrl = UrlParent + current.Attributes["href"].Value;
                var itemName = GetNameOfCity(current.InnerText);
                var city = new CityWithRestaurants { Name = itemName, Url = itemUrl };
                ListOfCityFromSite.Add(city);
            }
        }

        private void GetUrlToRestaurantsInCityFromFirstPage(string url)
        {
            var web = new HtmlWeb();
            var doc = web.Load(url);

            var htmlCollection = doc.DocumentNode.SelectNodes("//div[@id='LOCATION_LIST']/div[@id='BROAD_GRID']/div[@class='geos_grid']/div[@class='geos_row']/div[@class='geo_wrap']/div[@class='geo_entry']/div[@class='geo_info']/div[@class='geo_name']/a");
            foreach (var current in htmlCollection)
            {
                var itemUrl = UrlParent + current.Attributes["href"].Value;
                var itemName = GetNameOfCity(current.InnerText);
                var city = new CityWithRestaurants { Name = itemName, Url = itemUrl };
                ListOfCityFromSite.Add(city);
            }
        }

        private int TryConvertToInt(string value)
        {
            int convertedValue;
            var onlyDigits = Regex.Replace(value, "[^0-9]", "");
            try
            {
                convertedValue = System.Convert.ToInt32(onlyDigits);
            }
            catch (Exception e)
            {
                throw e;
            }
            return convertedValue;
        }

        private double TryConvertToDouble(string value)
        {
            double convertedValue;
            var onlyDigits = Regex.Replace(value, "[^0-9,]", "");
            try
            {
                convertedValue = System.Convert.ToDouble(onlyDigits);
            }
            catch (Exception e)
            {
                throw e;
            }
            return convertedValue;
        }

        private double TryConvertToDoubleWithDot(string value)
        {
            double convertedValue;
            var onlyDigits = Regex.Replace(value, "[^0-9.]", "");
            var valueWithoutDots = Regex.Replace(value, "[.]", ",");

            try
            {
                convertedValue = System.Convert.ToDouble(valueWithoutDots);
            }
            catch (Exception e)
            {
                throw e;
            }
            return convertedValue;
        }
    }
}