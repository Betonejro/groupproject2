﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Ajax.Utilities;
using MongoDB.Bson;
using MongoDB.Driver.GeoJsonObjectModel;

namespace RestAPI.Models
{
    public class Restaurant
    {
        public ObjectId id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
      
        public List<string> Kuchnia = new List<string>();
        public double Rate { get; set; }
        public string Url { get; set; }
        public int Voices { get; set; }

        public double LatT { get; set; }
        public double LongT { get; set; }
        
        public string CheckTheName(string name)
        {
            if(string.IsNullOrEmpty(name))
            {
                throw new Exception();
            }
            else
            {
                return name;
            }
        }

        public double CheckRate(double rate)
        {
            if((rate>=0)&&(rate<=10))
            {
                return rate;
            }
            else
            {
                throw new Exception();
            }

        }

        public GeoJsonPoint<GeoJson2DGeographicCoordinates> Location { get; set; }

        MongoCRUD mongoCRUD = new MongoCRUD("Restauracje");
        

        public List<Restaurant> findRestaurantInMaxDistance(int distance , double newlat , double newlong)
        {
            List<Restaurant> restaurants = new List<Restaurant>();

            foreach (var item in mongoCRUD.findTheNeighbors(distance, newlat, newlong))
            {
                restaurants.Add(item);
            }
          
            return restaurants;
        }
     



    }
}