﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using System.Xml.Serialization;
using MongoDB.Driver.GeoJsonObjectModel;
using Antlr.Runtime.Tree;
using System.Web.Http;
using Marten;

namespace RestAPI.Models
{
    public class MongoCRUD
    {

        private IMongoDatabase db;
        public MongoCRUD(string database)
        {
            var client = new MongoClient();
            db = client.GetDatabase(database);
        }

        public void InsertRecord<T>(string table, T record)
        {
            var collection = db.GetCollection<T>(table);
            collection.InsertOne(record);
        }

        public bool CheckCityInCollection(string collectionName, string primaryKey)
        {
            var collection = db.GetCollection<CityWithRestaurants>(collectionName);
            var isExist = collection.AsQueryable().FirstOrDefault(tag => tag.Url == primaryKey) != null;
            return isExist;
        }

        public bool CheckRestaurantInCollection(string collectionName, string primaryKey)
        {
            var collection = db.GetCollection<Restaurant>(collectionName);
            var isExist = collection.AsQueryable().FirstOrDefault(tag => tag.Url == primaryKey) != null;
            return isExist;
        }

        public long GetCountOfCity(string table)
        {
            var collection = db.GetCollection<CityWithRestaurants>(table);
            var countOfCity = collection.AsQueryable().Count();
            return countOfCity;
        }

        public List<CityWithRestaurants> GetCityWithRestaurants<t>(string table)
        {
            var collection = db.GetCollection<CityWithRestaurants>(table);
            var data = collection.Find(x => true).ToList();

            return data;
        }

        public List<T> ReturnXRSSItems<T>(string table, int count)
        {
            if (count <= 0) throw new Exception("returnXRSSItems wrong count");

            var collection = db.GetCollection<T>(table);
            List<T> itemList = new List<T>();
            foreach (var item in collection.Find(x => true).Limit(count).ToList())
            {
                itemList.Add(item);
            }

            return itemList;
        }
        public List<CityWithRestaurants> returnCityWithRestaurantsFindedByCity<t>(string table, string city)
        {

            var filter = Builders<CityWithRestaurants>.Filter.Eq("Name", city);
            var collection = db.GetCollection<CityWithRestaurants>(table);
            var ToReturn = collection.Find(filter).ToList();
           
            return ToReturn;
        }

        [Obsolete]
        public void createIndexForCoordinates()
        {

            IMongoCollection<Restaurant> collection = db.GetCollection<Restaurant>("Restauracje");
            collection.Indexes.CreateOne(new IndexKeysDefinitionBuilder<Restaurant>().Geo2DSphere(x => x.Location));
        }

        [Obsolete]
        public void InsertIndexedData(Restaurant record)
        {
            IMongoCollection<Restaurant> collection = db.GetCollection<Restaurant>("Restauracje");
            collection.Indexes.CreateOne(new IndexKeysDefinitionBuilder<Restaurant>().Geo2DSphere(x => x.Location));

            collection.InsertOne(record);

        }

        public List<Restaurant>ReturnALL()
        {
            List<Restaurant> restaurants = new List<Restaurant>();
            IMongoCollection<Restaurant> collection = db.GetCollection<Restaurant>("Restauracje");
            

            return collection.AsQueryable().ToList();

        }
        public List<Restaurant> findTheNeighbors(int distance , double latt , double longt)
        {
            List<Restaurant> restaurants = new List<Restaurant>();
            IMongoCollection<Restaurant> collection = db.GetCollection<Restaurant>("Restauracje");

            var point = GeoJson.Point(new GeoJson2DGeographicCoordinates(latt, longt));
            
            
            IAsyncCursor<Restaurant> cursor = collection.FindSync(new FilterDefinitionBuilder<Restaurant>().Near(x => x.Location, point, maxDistance: distance));
            var hasNeighbors = cursor.ToEnumerable();
            foreach (var item in hasNeighbors)
            {
                restaurants.Add(item);
            }
            return restaurants;
        }
    
        public List<Restaurant> findByKitchenTypeOne(string kitchen)
        {
            List<Restaurant> restaurants = new List<Restaurant>();

            var collection = db.GetCollection<Restaurant>("Restauracje");

            var filter = Builders<Restaurant>.Filter.Eq("Kuchnia", kitchen);

            var ToReturn = collection.Find(filter).ToList();

            foreach (var item in ToReturn)
            {
                restaurants.Add(item);
            }

            return restaurants;

        }


        //https://localhost:44336/api/restaurant/GetByRestaurantType?kitchens=Azjatycka&kitchens=Grill
        public List<Restaurant> findByKitchenType(List<string> kitchens)
        {
            List<Restaurant> restaurants = new List<Restaurant>();

            var collection = db.GetCollection<Restaurant>("Restauracje");

            foreach (var item in kitchens)

            {

                var filter = Builders<Restaurant>.Filter.Eq("Kuchnia", item);

                var ToReturn = collection.Find(filter).ToList();

                foreach (var inside in ToReturn)

                {

                    restaurants.Add(inside);

                }

            }


            return restaurants;
        }
        public List<Restaurant> findByKitchenTypeAndDistance(List<string> kitchens , List<Restaurant> FindedRestaurans)
        {   
            List<Restaurant> restaurants = new List<Restaurant>();
            List<Restaurant> ToReturn = new List<Restaurant>();

            foreach (var restaurant in FindedRestaurans)
            {
                foreach (var simple in restaurant.Kuchnia)
                {
                    foreach (var item in simple)
                    {
                        foreach (var type in FindedRestaurans)
                        {
                            if(item.ToString()==type.ToString())
                            {
                                restaurants.Add(restaurant);
                            }
                        }
                    }

                }




            }

            foreach (var item in findByKitchenType(kitchens))
            {
                restaurants.Add(item);
            }
         
          
            return restaurants;
        }

        public List<string> ReturnAllTypesOfDishes()
        {
            
                List<string> types = new List<string>();

                var collection = db.GetCollection<Restaurant>("Restauracje").AsQueryable();

            foreach (var item in collection)
            {
                foreach (var type in item.Kuchnia)
                {
                    types.Add(type);
                }
            }


            return types;

        }



        }
}



