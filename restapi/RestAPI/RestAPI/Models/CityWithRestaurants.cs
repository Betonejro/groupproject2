﻿using MongoDB.Bson;

namespace RestAPI.Models
{
    public class CityWithRestaurants
    {

        public ObjectId id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
    }
}