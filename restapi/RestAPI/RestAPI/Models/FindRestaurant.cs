﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RestAPI.Models
{
    public class FindRestaurant
    {
        public double LongT { get; set; }
        public double LatT { get; set; }

        MongoCRUD mongoCRUD = new MongoCRUD("Restauracje");
        Restaurant restaurant = new Restaurant();

        public List<Restaurant> findRestaurantInMaxDistance(int distance)
        {
            List<Restaurant> restaurants = new List<Restaurant>();
            
            foreach (var item in mongoCRUD.findTheNeighbors(distance, LatT, LongT))
            {
                restaurants.Add(item);
            }
            return restaurants;
        }
        public void whereIam(double LongT, double LatT)
        {
            this.LongT = LongT;
            this.LatT = LatT;
        }



    }
}