﻿using Marten;
using Microsoft.Ajax.Utilities;
using MongoDB.Bson;
using MongoDB.Bson.IO;
using RestAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web.Http;

namespace RestAPI.Controllers
{
    public class ValuesController : ApiController
    {

        public ValuesController()
        {

        }
        MongoCRUD mongoCRUD = new MongoCRUD("RestAPI");
        RestaurantFinder rf = new RestaurantFinder();

        /// <summary>
        /// Funkcja Pomocnicza
        /// </summary>
        /// <param name="restaurants"></param>
        /// <returns></returns>
        public List<Restaurant> AddToList(List<Restaurant> restaurants, Restaurant restaurant)
        {
            restaurants.Add(restaurant);
            return restaurants;
        }
        public int CheckTheDistanceParametr(int distance)
        {
            if(distance==0)
            {
                throw new Exception("Distance should be bigger than 0");
            }

            if ((distance < int.MaxValue) || (distance > int.MinValue))
            {
                return distance;
            }
            else
            {
                throw new Exception();
            }
        
                    
        }

        public double CheckTheLattParametr(double latt)
        {
            if ((latt <= 90) &&(latt >= 0))
            {
                return latt;
            }
            else
            {
                throw new Exception("Latt param is bad");
            }
        }

        public double CheckTheLongTParametr(double longt)
        {
            if ((longt <= 90) && (longt >= 0))
            {
                return longt;
            }
            else
            {
                throw new Exception("LongT param is bad");

            }
        }

        public string CheckCorrectOfKitchen(string kitchen)
        {
            if(string.IsNullOrEmpty(kitchen))
            {
                throw new Exception();
            }
            else
            {
                return kitchen;
            }
        }

       




        /// <summary>
        /// Zwraca wszystkie restauracje dostępne w bazie
        /// </summary>
        /// <returns></returns>

        [Route("api/restaurant/Getall")]
        [HttpGet]
        public List<Restaurant> GetAll()
        {
            List<Restaurant> restaurants = new List<Restaurant>();

            

            return mongoCRUD.ReturnALL();

        }
    


        /// <summary>
        /// Zwraca wszystkie restauracja w zadanym dystansie, przyjmuje metry 
        /// </summary>
        /// <returns></returns>
        [Route("api/restaurant/GetByDistance/{latt:double}/{longt:double}/{distance:int}")]
        [HttpGet]
        public List<Restaurant> GetByDistance(int distance, double latt, double longt)
        {
               return mongoCRUD.findTheNeighbors(CheckTheDistanceParametr(distance), latt, longt);
     
        }

        /// <summary>
        /// Zwraca wszystkie restauracje o podabyn typie kuchni
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        [Route("api/restaurant/GetByRestaurantTypeOne/{type}")]
        [HttpGet]
        public List<Restaurant> GetByRestaurantType(string type)
        {
            List<Restaurant> restaurants = new List<Restaurant>();


            foreach (var item in mongoCRUD.findByKitchenTypeOne(type))
            {
                restaurants.Add(item);
            }

            return restaurants;

        }
       /// <summary>
       /// przyjmuje wiele typów restauracji i zwraca tylko te które spełniają je wszystki 
       /// </summary>
       /// <param name="kitchens"></param>
       /// <returns></returns>
        [Route("api/restaurant/GetByRestaurantType")]
        [HttpGet]
        public List<Restaurant> GetByRestaurantType([FromUri] List<string> kitchens)
        {
            List<Restaurant> restaurants = new List<Restaurant>();
           // kitchens.Add("Azjatycka");


            foreach (var item in mongoCRUD.findByKitchenType(kitchens))
            {
                restaurants.Add(item);
            }

            return restaurants;

        }
        //[Route("api/restaurant/GetByDistance/{latt:double}/{longt:double}/{distance:int}")]
        [Route("api/restaurant/GetByRestaurantTypeAndDistance/{latt:double}/{longt:double}/{distance:int}")]
        [HttpGet]
        public List<Restaurant> FindByDistanceAndCategory(double latt, double longt, int distance, [FromUri] List<string> kuchnie)
        {
            List<Restaurant> restaurants = new List<Restaurant>();
            List<Restaurant> ToReturn = new List<Restaurant>();
            foreach (var item in mongoCRUD.findTheNeighbors(distance,latt,longt))
            {
                restaurants.Add(item);
            }

            foreach (var item in mongoCRUD.findByKitchenTypeAndDistance(kuchnie,restaurants).Distinct())
            {
                ToReturn.Add(item);
            }
            return ToReturn;

        }
        [Route("api/restaurant/ReturnAllTypesOfKitchen")]
        [HttpGet]
        public List<string> ReturnAllTypesOfKitchen()
        {
            List<string> types = new List<string>();
            foreach (var item in mongoCRUD.ReturnAllTypesOfDishes().Distinct())
            {
               types.Add(item);

            }
            return types;
        
        }


        [Route("api/restaurant/findByDistanceAndReturnRawJSON/{latt}/{longt}/{distance}")]
        [HttpGet]
        public HttpResponseMessage findByDistanceAndReturnRawJSON(double latt, double longt, int distance)
        {
            List<Restaurant> restaurants = new List<Restaurant>();

            foreach (var item in mongoCRUD.findTheNeighbors(distance, latt, longt))
            {
                restaurants.Add(item);
            }
            var rawJSON = restaurants.ToJson();
            //string rawJsonFromDb = _session.Query<Restaurant>().ToJsonArray();
            var q = Request.CreateResponse(HttpStatusCode.OK);
            q.Content = new StringContent(rawJSON, Encoding.UTF8, "application/json");

            return q;
        }


        /// <summary>
        ///  Przykładowe użycie 
        ///  https://localhost:44336/api/restaurant/getByRestaurantTypeAndReturnRawJSON?kitchens=Polska
        /// </summary>
        /// <param name="kitchens"></param>
        /// <returns></returns>
        [Route("api/restaurant/getByRestaurantTypeAndReturnRawJSON")]
        [HttpGet]
        public HttpResponseMessage getByRestaurantTypeAndReturnRawJSON([FromUri] List<string> kitchens)
        {
            List<Restaurant> restaurants = new List<Restaurant>();
            // kitchens.Add("Azjatycka");


            foreach (var item in mongoCRUD.findByKitchenType(kitchens))
            {
                restaurants.Add(item);
            }

            var rawJSON = restaurants.ToJson();
            //string rawJsonFromDb = _session.Query<Restaurant>().ToJsonArray();
            var q = Request.CreateResponse(HttpStatusCode.OK);
            q.Content = new StringContent(rawJSON, Encoding.UTF8, "application/json");

            return q;
        }
        /// <summary>
        /// Przykładowe użycie 
        /// https://localhost:44336/api/restaurant/GetByRestaurantTypeAndDistanceAndReturnRawJSON/52.20282/21.07433/5?kuchnie=Azjatycka&kuchnie=Grill
        /// </summary>
        /// <param name="latt"></param>
        /// <param name="longt"></param>
        /// <param name="distance"></param>
        /// <param name="kuchnie"></param>
        /// <returns></returns>
        [Route("api/restaurant/GetByRestaurantTypeAndDistanceAndReturnRawJSON/{latt:double}/{longt:double}/{distance:int}")]
        [HttpGet]
        public HttpResponseMessage FindByDistanceAndCategoryAndReturnRawJSON(double latt, double longt, int distance, [FromUri] List<string> kuchnie)
        {
            List<Restaurant> restaurants = new List<Restaurant>();
            List<Restaurant> ToReturn = new List<Restaurant>();
            foreach (var item in mongoCRUD.findTheNeighbors(distance, latt, longt))
            {
                restaurants.Add(item);
            }

            foreach (var item in mongoCRUD.findByKitchenTypeAndDistance(kuchnie, restaurants).Distinct())
            {
                ToReturn.Add(item);
            }
           
            var rawJSON = restaurants.ToJson();

            var q = Request.CreateResponse(HttpStatusCode.OK);
            q.Content = new StringContent(rawJSON, Encoding.UTF8, "application/json");

            return q;

        }
        /// <summary>
        /// przykładowe użycie
        /// https://localhost:44336/api/restaurant/ReturnAllTypesOfKitchenAndReturnRawJSON
        /// </summary>
        /// <returns></returns>
        [Route("api/restaurant/ReturnAllTypesOfKitchenAndReturnRawJSON")]
        [HttpGet]
        public HttpResponseMessage ReturnAllTypesOfKitchenAndReturnRawJSON()
        {
            List<string> types = new List<string>();
            foreach (var item in mongoCRUD.ReturnAllTypesOfDishes().Distinct())
            {
                types.Add(item);

            }
            

            var rawJSON = types.ToJson();

            var q = Request.CreateResponse(HttpStatusCode.OK);
            q.Content = new StringContent(rawJSON, Encoding.UTF8, "application/json");

            return q;
        }



        [Route("api/restaurant/Test")]
        [HttpPost]
        public void Test([FromUri] List<string> kitchens)
        {
         
            var kuchnia = "Kremowkarnia";
            var NewRestaurant = new Restaurant();
            NewRestaurant.Name = "KremówkarskaDolina";
            NewRestaurant.Address = "JanaPawłaDrugiego";
          
            NewRestaurant.Rate = 5;
            NewRestaurant.Kuchnia.Add(kuchnia);
            NewRestaurant.Url = @"https://www.google.pl/search?q=cenzopapa&sxsrf=ALeKk03g94BQmqLQpO_0gEXaEsHI4S0hoA:1589756755438&tbm=isch&source=iu&ictx=1&fir=x54PCvkyfK5MCM%253A%252Cy5nBC_fhNbrJyM%252C_&vet=1&usg=AI4_-kTsSAYq9SezlAQs4IGprE3Ixm5Idg&sa=X&ved=2ahUKEwjVnr-1gbzpAhUr4KYKHUY2BTQQ9QEwCHoECAoQIA#imgrc=x54PCvkyfK5MCM:";
            NewRestaurant.Voices = 4;
            NewRestaurant.LatT = 52.25293;
            NewRestaurant.LongT = 21.00773;

            mongoCRUD.InsertIndexedData(NewRestaurant);
        }


    }
}
