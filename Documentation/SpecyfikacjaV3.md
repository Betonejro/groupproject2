 # **Aplikacja I'm Hungry!**
## Pozwalająca na szybką i komplekową lokalizację restauracji znajdujących się w pobliżu użytkownika.
### *Specyfikacja wymagań Projektowych*


*******


 ## 1.1  Wprowadzenie
  *Dokumentacja stanowiąca podstawę zdefiniowanych wymagań jawnych i niejawnych wynikająych z potrzeb które aplikacja ma za zadanie spełniać.*

 ## 1.2 Cel
  *Dokument ten, ma za zadanie sprecyzowac listę funkcjonalności wynikających z użytkowania aplikacji oraz zdefiniować w jaki sposób powinny dzialać poszczególne funkcje aplikacji.*   

 ## 1.3 Zakres
  *Dokumentacja zawiera wpływające na poprawne dzialanie czynniki mające na celu określić zakres wymagań jakie aplikacja ma spełniać przy zachowaniu wszystkich ograniczeń. Zachowując czynniki wpływające na końcowy produkt takie jak czas, liczbę użytkowników, wymagane środowiska programistyczne, przepisy licencyjne poszczególnych składowych programów mających wpływ na ograniczenia w działaniu apikacji.*  
## 1.4 Definicje

* ***Aplikacja***- samodzielny program lub element pakietu oprogramowania, który nie jest zaliczany do oprogramowania systemowego lub programów usługowych.
* ***System***- zespół wzajemnie powiązanych elementów realizujących jako całość założone cele.
* ***Interfejs***- przestrzeń, w której następuje interakcja człowieka z programem.
* ***Baza Danych***-  zorganizowany zbiór uporządkowanych informacji, czyli danych, zwykle przechowywany w systemie komputerowym w formie elektronicznej.
* ***Użytkownik***- osoba korzystająca z funkcji aplikacji.
* ***Lokalizacja***- to miejsce, w którym ma się znaleźć lub znajduje się jakiś obiekt




**************************


## 2. Opis ogólny

### Głównym założeniem aplikacji jest lokalizacja na podstawie aktualnego położenia użytkownika wszystkich, okolicznych barów, restauracji oraz innych miejsc gastronoomicznych znajdujących się w bazie danych serwisu **TripAdvisor**. 

### Aplikacja w sposób automatyczny będzie przedstawiała znajdujące się w określonym przez użytkowika okręgu lokale gastronomiczne, ich ocenę, odległośc od użytkowika oraz status lokalu. Właściwością aplikacji będzie możlwiość filtracji restauracji w zależnosci od preferowanych przez użytkownika upodobań wynikających z danej kuchni, oceny restauracji czy odległości od aktualnego położenia. 

### Mając na uwadzę że aplikacja ma działać w sposób błyskawiczny, całość zbędnego zaplecza takiego jak logowanie czy uwieżytelnianie osoby korzystającej z aplikacji będzie pominięte, tak aby użytkownik miał pełną swobodę i niewymagane było od niego jakiekolwiek działanie mogące wydłużyć czas dostępu do aplikacji i jej zasobów. 

### Użytkownik korzystający z aplikacji *I'm hungry!* będzie miał możliwość oceny odwiedzanych punktów gastronomicznych, z których korzystał.


**************************
## 3. Wymagania funkcjonalne

1. Użytkownik za pomocą aplikacji ma możliwość wyszukania punktu gastronomicznego w najbliższej okolicy, w której się znajduję.
2. Aplikacja daje możliwość użytkownikowi recenzowania odwiedzonych restauracji.
3. Użytkownik korzystający z aplikacji może określić specyfikację kuchni restauracji której szuka.
4. Użytkownik ma możliwość wyznaczenia zakresu odległości w której chce znaleźć restauracje
5. Użytkownik może przeglądać ocenione przez siebie restauracje.

***

## 4. Wymagania niefunkcjonalne
1. Aplikacja musi mieć przejrzysty interfejs, oprogramowanie powinno być przyjazne dla użytkownika.
2. Wykorzystanie API geocode.xyz w celu wyciągnięcia usytuowania restauracji
3. Wykorzystanie własnego API które komunikuje się z aplikacją mobilną i bazą danych
4. Wykorzystane zostaną funkcje w MongoDB które służą do obliczania odległości pomiędzy dwoma punktami.
5. Bot który pobiera dane z serwisu TripAdvisor


## 5. Opis wybranej technologii
Do napisania naszej aplikacji będziemy posługiwali się Flutterem. Aplikacje Flutter pisze się w Dart. Dart jest to obiektowy język z systemem typów, który łączy statyczne typowanie wraz z kontrolą w *runtime*. Zezwala on na inferencję typów, dzięki czemu można pomijać ich deklaracje. Wykorzystując Fluttera będziemy działać na widgetach. Flutter daje nam możliwość korzystania z pól tekstowych z tytułami, wrapperów czy też przycisków.
Flutter posiada mechanizm *async/await*, który wraz z typem Future pozwala na pisanie asynchronicznego kodu tak, jakby był on kodem synchronicznym

 Google zaleca korzystać z architektury *BLoC(eng.Bussiness Logic Component)* gdzie stosuje się osobne klasy. Klasy te komunikują się ze sobą asynchronicznie. Oznacza to, że interfejs który publikuje pewne zdarzenia w wejściach naszej architektury BLoC również sprawdza wyjścia w których znajdują się kolejne zmiany aplikacji. Stosując widget StreamBuilder możemy odbierać dane ze strumieni.
Architekturę taką stosuję się w aplikacjach, w których dane w dowolnej kolejności w dowolnych momentach napływają do programu użytkowego.
* * *
W projekcie korzystaliśmy z *Rest Api* który może obsługiwać wiele rodzai połączeń i pracuje z *JSON*. Rest jest to styl architektury dla rozproszonych systemów hipermedialnych. REST używa identyfikatora zasobu do identyfikacji konkretnego zasobu zaangażowanego w interakcję między komponentami. Wszelakie informacje, obiekty, obrazy w Rest są zasobami.

Najważniejsze zasady w Rest: 

* Client-Server – architektura typu Klient – Serwer.
* Cache – buforowanie danych po stornie klienta.
* Stateless – poszczególne połączenia są od siebie niezależne.
* Uniform Interface – separacja implementacji po stronie serwera i klienta poprzez zastosowanie jednolitego interfejsu.
* Layered System – dzielnie architektury REST na współdziałające ze sobą warstwy.
* Code on Demand – pozwala na przesyłanie nowych fragmentów kodu poprzez api.
***


## Diagram struktury aplikacji

![alt text](https://gitlab.com/Betonejro/groupproject2/-/raw/ImmBoo/Diagram_I_m_Hungry.png "Diagram Struktury Aplikacji")

